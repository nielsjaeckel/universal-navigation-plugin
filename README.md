## Universal Navigation Plugin for Confluence ##

This plugin allows to add a main navigation to [Atlassian Confluence](https://www.atlassian.com/software/confluence). The basis is an easy-to-understand XML file that is uploaded in the administration panel. Each structure consists of navigation nodes of a certain type, e.g.

* Space link
* Page link
* External link
* Link to Confluence action

Additionally there are special node types, such as

* Text node (no link, only for structuring)
* List of user's favorite pages

These types are bundled with the plugin. If you need a special node, you can write your own node types via a plugin.