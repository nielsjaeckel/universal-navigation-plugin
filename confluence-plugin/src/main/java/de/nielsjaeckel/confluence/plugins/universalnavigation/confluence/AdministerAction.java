package de.nielsjaeckel.confluence.plugins.universalnavigation.confluence;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.gzipfilter.org.apache.commons.lang.StringUtils;
import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.webwork.dispatcher.multipart.MultiPartRequestWrapper;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.ApplicationIntegrationService;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.Navigation;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.NodeContext;
import de.nielsjaeckel.confluence.plugins.universalnavigation.service.PersistenceService;
import de.nielsjaeckel.confluence.plugins.universalnavigation.service.RendererRegistry;
import de.nielsjaeckel.confluence.plugins.universalnavigation.service.XmlNavigationService;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.regex.Pattern;

public class AdministerAction extends ConfluenceActionSupport {

    private static final Logger LOG = LoggerFactory.getLogger(AdministerAction.class);

    private static final String FILE_UPLOAD_FIELD = "newNavigation";
    private static final String ASSUMED_XML_FILE_ENCODING = "UTF-8";

    private String newNavigationStyle;
    private static final String STYLE_INVALID_REGEX = "[^a-zA-Z-]";

    private PersistenceService persistenceService;
    private ApplicationIntegrationService applicationIntegrationService;
    private XmlNavigationService xmlNavigationService;
    private RendererRegistry rendererRegistry;

    private Navigation currentMainNavigation;

    public String uploadMainNavigation() {

        InputStream fis = null;
        try {

            HttpServletRequest request = ServletActionContext.getRequest();
            MultiPartRequestWrapper multiPartRequest = (MultiPartRequestWrapper) request;
            File[] newNavigationFiles = multiPartRequest.getFiles(FILE_UPLOAD_FIELD);

            if (newNavigationFiles.length > 0) {

                File uploadedFile = newNavigationFiles[0];
                fis = new FileInputStream(uploadedFile);
                persistenceService.storeMainNavigation(fis);
                addActionMessage("unp.admin.manage.upload.success");
            }

        } catch (Exception e) {

            LOG.error("Error during file upload", e);
            addActionError(getText("unp.admin.manage.upload.error", new Object[]{e.getMessage()}));
            return ERROR;
        }
        finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    LOG.error("Error while closing uploaded file.", e);
                }
            }
        }

        return SUCCESS;
    }


    public String setMainNavigationStyle() {

        // check not empty
        if (StringUtils.isEmpty(newNavigationStyle)) {
            addActionError(getText("unp.admin.manage.style.action.error.empty"));
            return ERROR;
        }

        // check valid characters
        if (Pattern.compile(STYLE_INVALID_REGEX).matcher(newNavigationStyle).find()) {
            addActionError(getText("unp.admin.manage.style.action.error.invalid-characters") + " " + getText("unp.admin.manage.current.style.valid-characters"));
            return ERROR;
        }

        persistenceService.setMainNavigationStyle(newNavigationStyle);

        return SUCCESS;
    }


    public String deleteMainNavigation() {

        try {
            persistenceService.deleteMainNavigation();
        }
        catch (Exception e) {

            LOG.error("Could not delete main navigation", e);
            addActionError(getText("unp.admin.manage.delete.error", new Object[]{e.getMessage()}));
            return ERROR;
        }

        return SUCCESS;
    }


    public Navigation getCurrentMainNavigation() {

        if (currentMainNavigation == null) {
            loadMainNavigation();
        }

        return currentMainNavigation;
    }


    public String getCurrentMainNavigationXml() {

        InputStream xmlStream = null;
        try {
            xmlStream = persistenceService.readMainNavigation();
            StringWriter writer = new StringWriter();
            IOUtils.copy(xmlStream, writer, ASSUMED_XML_FILE_ENCODING);
            return writer.toString();

        } catch (IOException e) {
            LOG.error("Could not read main navigation from file system.", e);
            return e.toString();
        }
        finally {

            if (xmlStream != null) {
                try {
                    xmlStream.close();
                } catch (IOException e) {
                    LOG.error("Could not close file after reading main navigation from file system.", e);
                }
            }
        }
    }


    private void loadMainNavigation() {

        currentMainNavigation = null;
        InputStream mainNavigationStream = null;

        try {

            mainNavigationStream = persistenceService.readMainNavigation();

            if (mainNavigationStream != null) {

                NodeContext nodeContext =  applicationIntegrationService.getCurrentNodeContext();
                currentMainNavigation = xmlNavigationService.parseXmlNavigation(mainNavigationStream, nodeContext, XmlNavigationService.ParseMode.LENIENT);
            }
        }
        catch (Exception e) {
            addActionError(getText("unp.admin.manage.current.error", new Object[]{e.getMessage()}));
            LOG.error("Cannot parse main navigation!", e);
        }
        finally {
            if (mainNavigationStream != null) {
                try {
                    mainNavigationStream.close();
                } catch (IOException e) {
                    LOG.error("Error closing main navigation XML stream.", e);
                }
            }
        }
    }

    public String getMainNavigationStyle() {
        return persistenceService.getMainNavigationStyle();
    }


    public void setNewNavigationStyle(String newNavigationStyle) {
        this.newNavigationStyle = newNavigationStyle;
    }

    public void setPersistenceService(PersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    public void setApplicationIntegrationService(ApplicationIntegrationService applicationIntegrationService) {
        this.applicationIntegrationService = applicationIntegrationService;
    }

    public void setXmlNavigationService(XmlNavigationService xmlNavigationService) {
        this.xmlNavigationService = xmlNavigationService;
    }

    public void setRendererRegistry(RendererRegistry rendererRegistry) {
        this.rendererRegistry = rendererRegistry;
    }
}