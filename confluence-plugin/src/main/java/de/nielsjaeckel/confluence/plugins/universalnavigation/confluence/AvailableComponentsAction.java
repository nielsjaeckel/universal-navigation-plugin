package de.nielsjaeckel.confluence.plugins.universalnavigation.confluence;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.NavigationRenderer;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.NodeType;
import de.nielsjaeckel.confluence.plugins.universalnavigation.service.NodeTypeRegistry;
import de.nielsjaeckel.confluence.plugins.universalnavigation.service.RendererRegistry;

import java.util.*;

public class AvailableComponentsAction extends ConfluenceActionSupport {

    //private static final Logger LOG = LoggerFactory.getLogger(AdministerAction.class);
    private RendererRegistry rendererRegistry;
    private NodeTypeRegistry nodeTypeRegistry;


    public List<NavigationRenderer> getNavigationRenderers() {
        List<NavigationRenderer> renderers = new ArrayList<NavigationRenderer>(rendererRegistry.getRegisteredRenderers());
        Collections.sort(renderers, new NavigationRendererComparator());
        return renderers;
    }

    public List<NodeType> getNodeTypes() {
        List<NodeType> nodeTypes = new ArrayList<NodeType>(nodeTypeRegistry.getRegisteredNodeTypes());
        Collections.sort(nodeTypes, new NodeTypeComparator());
        return nodeTypes;
    }

    public void setRendererRegistry(RendererRegistry rendererRegistry) {
        this.rendererRegistry = rendererRegistry;
    }

    public void setNodeTypeRegistry(NodeTypeRegistry nodeTypeRegistry) {
        this.nodeTypeRegistry = nodeTypeRegistry;
    }


    public static class NavigationRendererComparator implements Comparator<NavigationRenderer> {

        @Override
        public int compare(NavigationRenderer r1, NavigationRenderer r2) {
            return r1.getId().compareTo(r2.getId());
        }
    }


    public static class NodeTypeComparator implements Comparator<NodeType> {

        @Override
        public int compare(NodeType n1, NodeType n2) {
            return n1.getId().compareTo(n2.getId());
        }
    }
}
