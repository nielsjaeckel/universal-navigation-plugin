package de.nielsjaeckel.confluence.plugins.universalnavigation.confluence;


import com.atlassian.plugin.web.model.WebPanel;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.spring.container.ContainerManager;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.*;
import de.nielsjaeckel.confluence.plugins.universalnavigation.service.PersistenceService;
import de.nielsjaeckel.confluence.plugins.universalnavigation.service.RendererRegistry;
import de.nielsjaeckel.confluence.plugins.universalnavigation.service.XmlNavigationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.util.Map;

public class MainNavigationWebPanel implements WebPanel {

    private static final Logger LOG = LoggerFactory.getLogger(MainNavigationWebPanel.class);

    private static final String PANEL_TEMPLATE = "<div id=\"universal-navigation-main-navigation\">%s</div>";
    private static final String NAVIGATION_RENDERER_ID = "default";

    private PersistenceService persistenceService;
    private ApplicationIntegrationService applicationIntegrationService;
    private XmlNavigationService xmlNavigationService;
    private RendererRegistry rendererRegistry;

    private WebResourceIntegration webResourceIntegration;


    @Override
    public String getHtml(Map<String, Object> stringObjectMap) {

        String navigationHtml = null;
        InputStream mainNavigationStream = null;

        try {

            mainNavigationStream = persistenceService.readMainNavigation();

            if (mainNavigationStream != null) {

                NodeContext nodeContext = getNodeContext();
                Navigation navigation = xmlNavigationService.parseXmlNavigation(mainNavigationStream, nodeContext, XmlNavigationService.ParseMode.LENIENT);
                NavigationRenderer navigationRenderer = rendererRegistry.getRenderer(NAVIGATION_RENDERER_ID);
                navigationHtml = navigationRenderer.renderNavigation(navigation, nodeContext, persistenceService.getMainNavigationStyle()).toString();
            }
        }
        catch (Exception e) {
            navigationHtml = e.getClass().getName() + ": " + e.getMessage();
        }
        finally {
            if (mainNavigationStream != null) {
                try {
                    mainNavigationStream.close();
                } catch (IOException e) {
                    LOG.error("Error closing main navigation XML stream.", e);
                }
            }
        }

        if (navigationHtml == null) {
            return "";
        }
        else {
            return String.format(PANEL_TEMPLATE, navigationHtml);
        }
    }


    private NodeContext getNodeContext() {

        NodeContext nodeContext = applicationIntegrationService.getCurrentNodeContext();

        WebResourceIntegration webResourceIntegration = (WebResourceIntegration) ContainerManager.getComponent("webResourceIntegration");
        Map<String, Object> requestCache = webResourceIntegration.getRequestCache();
        Map<String, String> confluenceMetadata = (Map<String, String>) requestCache.get("confluence.metadata.map");

        String spaceKey = confluenceMetadata.get("space-key");
        String pageId = confluenceMetadata.get("page-id");

        if (spaceKey != null && spaceKey != "" && pageId != null && pageId != "") {

            ((ConfluenceNodeContext) nodeContext).provideContext(spaceKey, pageId);
        }

        return nodeContext;
    }


    public void writeHtml(Writer writer, Map<String, Object> stringObjectMap) throws IOException {
        // currently nothing...
        // pre-cf 4.3 or so
    }

    public void setApplicationIntegrationService(ApplicationIntegrationService applicationIntegrationService) {
        this.applicationIntegrationService = applicationIntegrationService;
    }

    public void setPersistenceService(PersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    public void setXmlNavigationService(XmlNavigationService xmlNavigationService) {
        this.xmlNavigationService = xmlNavigationService;
    }

    public void setRendererRegistry(RendererRegistry rendererRegistry) {
        this.rendererRegistry = rendererRegistry;
    }
}
