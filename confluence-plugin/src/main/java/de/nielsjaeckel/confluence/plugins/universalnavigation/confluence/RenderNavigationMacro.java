package de.nielsjaeckel.confluence.plugins.universalnavigation.confluence;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.spaces.SpaceDescription;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.util.profiling.UtilTimerStack;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.*;
import de.nielsjaeckel.confluence.plugins.universalnavigation.service.RendererRegistry;
import de.nielsjaeckel.confluence.plugins.universalnavigation.service.XmlNavigationService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RenderNavigationMacro extends BaseMacro implements Macro {

    private static final Logger LOG = LoggerFactory.getLogger(RenderNavigationMacro.class);

    private static final String PARAM_ATTACHMENT_REGEX = "^\\^?([^\\^]+)$";
    private static final String PARAM_PAGE_ATTACHMENT_REGEX = "^([^\\^:]+)\\^([^\\^]+)$";
    private static final String PARAM_SPACE_PAGE_ATTACHMENT_REGEX = "^([^\\^:]+):([^\\^:]+)\\^([^\\^]+)$";

    private XmlNavigationService xmlNavigationService;
    private RendererRegistry rendererRegistry;
    private AttachmentManager attachmentManger;
    private PageManager pageManager;

    private ApplicationIntegrationService applicationIntegrationService;


    @Override
    public boolean hasBody() {
        return false;
    }


    @Override
    public RenderMode getBodyRenderMode() {
        return RenderMode.NO_RENDER;
    }


    @Override
    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }

    @Override
    public String execute(Map<String, String> parameters, String body, ConversionContext context)
            throws MacroExecutionException {
        try {
            if (context != null) {
                return execute(parameters, body, context.getPageContext());
            } else {
                return execute(parameters, body, (RenderContext) null);
            }

        } catch (MacroException e) {
            throw new MacroExecutionException(e.getMessage(), e);
        }
    }


    @Override
    public String execute(Map params, String body, RenderContext renderContext) throws MacroException {

        NavigationRenderer navigationRenderer = null;
        Attachment attachment = null;

        // get renderer
        String rendererId = "default";
        if (params.containsKey("renderer") && StringUtils.isNotBlank((String) params.get("renderer"))) {
            rendererId = (String) params.get("renderer");
        }

        if (rendererRegistry.hasRenderer(rendererId)) {
            navigationRenderer = rendererRegistry.getRenderer(rendererId);
        }
        else {
            throw new MacroException(String.format("The renderer '%s' is unknown.", rendererId));
        }


        // get attachment
        if (! params.containsKey("attachment") || StringUtils.isBlank((String) params.get("attachment"))) {
            throw new MacroException("Please provide the 'attachment'.");
        }

        String attachmentParameter = (String) params.get("attachment");

        // TODO: test this on space and dashboard level, too!
        attachment = getAttachment(attachmentParameter, ((PageContext) renderContext).getEntity());

        if (attachment == null) {

            throw new MacroException(String.format("Could not find attachment '%s'.", attachmentParameter));
        }


        // get style
        String style = null;
        if (params.containsKey("style") && StringUtils.isNotBlank((String) params.get("style"))) {
            style = (String) params.get("style");
        }

        String timerName = String.format("Rendering macro 'render-navigation' with renderer '%s' and attachment '%s'.", rendererId, attachmentParameter);
        UtilTimerStack.push(timerName);

        try {
            // get the node context
            NodeContext nodeContext = applicationIntegrationService.getCurrentNodeContext();

            // try to set context by RenderContext
            ContentEntityObject ceo = ((PageContext) renderContext).getEntity();
            if (ceo != null && ceo instanceof AbstractPage) {
                String spaceKey = ((AbstractPage) ceo).getSpaceKey();
                String pageId = Long.toString(((AbstractPage) ceo).getId());
                ((ConfluenceNodeContext) nodeContext).provideContext(spaceKey, pageId);
            }

            // optional: inject additional context infos
            if (params.containsKey("spaceKey")
                    && params.containsKey("pageId")
                    && StringUtils.isNotBlank((String) params.get("spaceKey"))
                    && StringUtils.isNotBlank((String) params.get("pageId"))) {

                String spaceKey = (String) params.get("spaceKey");
                String pageId = (String) params.get("pageId");
                ((ConfluenceNodeContext) nodeContext).provideContext(spaceKey, pageId);
            }

            // build navigation from attachment
            Navigation navigation;
            InputStream navigationStream = null;
            try {
                navigationStream = attachment.getContentsAsStream();
                navigation = xmlNavigationService.parseXmlNavigation(navigationStream, nodeContext, XmlNavigationService.ParseMode.LENIENT);
            }
            catch (Exception e) {
                LOG.error("Could not parse attachment: ", e);
                throw new MacroException("Could not parse attachment: " + e.getMessage(), e);
            }
            finally {
                if (navigationStream != null) {
                    try {
                        navigationStream.close();
                    } catch (IOException e) {
                        LOG.error("Error closing navigation XML stream.", e);
                    }
                }
            }

            String timerName2 = "executing NavigationRenderer.renderNavigation()";
            UtilTimerStack.push(timerName2);
            try {
                return navigationRenderer.renderNavigation(navigation, nodeContext, style).toString();
            }
            finally {
                UtilTimerStack.pop(timerName2);
            }
        }
        finally {
            UtilTimerStack.pop(timerName);
        }
    }


    private Attachment getAttachment(String attachmentParameter, ContentEntityObject ceo) throws MacroException {

        // first option: only attachment given, takes attachment of this page
        Pattern onlyAttachmentP = Pattern.compile(PARAM_ATTACHMENT_REGEX);
        Matcher onlyAttachmentM = onlyAttachmentP.matcher(attachmentParameter);
        if (onlyAttachmentM.find()) {

            // works only for pages and blog posts
            if (ceo instanceof AbstractPage) {

                String attachmentName = onlyAttachmentM.group(1);
                return attachmentManger.getAttachment(ceo, attachmentName);
            }
            else {

                // no PageContext
                throw new MacroException("Referencing an attachment only by name works only on pages and blog posts.");
            }
        }


        // second option: page and attachment given
        Pattern pageAttachmentP = Pattern.compile(PARAM_PAGE_ATTACHMENT_REGEX);
        Matcher pageAttachmentM = pageAttachmentP.matcher(attachmentParameter);
        if (pageAttachmentM.find()) {


            String spaceKey = null;
            String pageTitle = pageAttachmentM.group(1);
            String attachmentName = pageAttachmentM.group(2);

            if (ceo instanceof SpaceDescription) {

                spaceKey = ((SpaceDescription) ceo).getSpaceKey();
            }

            if (ceo instanceof AbstractPage) {

                spaceKey = ((AbstractPage) ceo).getSpaceKey();
            }

            if (spaceKey != null) {

                AbstractPage page = pageManager.getPage(spaceKey, pageTitle);
                return attachmentManger.getAttachment(page, attachmentName);
            }
            else {

                // no PageContext
                throw new MacroException(String.format("Could not determine space in which the page %s is located.", pageTitle));
            }
        }


        // third option: space, page and attachment given
        Pattern spacePageAttachmentP = Pattern.compile(PARAM_SPACE_PAGE_ATTACHMENT_REGEX);
        Matcher spacePageAttachmentM = spacePageAttachmentP.matcher(attachmentParameter);
        if (spacePageAttachmentM.find()) {

            String spaceKey = spacePageAttachmentM.group(1);
            String pageTitle = spacePageAttachmentM.group(2);
            String attachmentName = spacePageAttachmentM.group(3);

            AbstractPage page = pageManager.getPage(spaceKey, pageTitle);
            return attachmentManger.getAttachment(page, attachmentName);
        }


        return null;
    }

    public void setXmlNavigationService(XmlNavigationService xmlNavigationService) {
        this.xmlNavigationService = xmlNavigationService;
    }

    public void setRendererRegistry(RendererRegistry rendererRegistry) {
        this.rendererRegistry = rendererRegistry;
    }

    public void setAttachmentManger(AttachmentManager attachmentManger) {
        this.attachmentManger = attachmentManger;
    }

    public void setPageManager(PageManager pageManager) {
        this.pageManager = pageManager;
    }

    public void setApplicationIntegrationService(ApplicationIntegrationService applicationIntegrationService) {
        this.applicationIntegrationService = applicationIntegrationService;
    }
}