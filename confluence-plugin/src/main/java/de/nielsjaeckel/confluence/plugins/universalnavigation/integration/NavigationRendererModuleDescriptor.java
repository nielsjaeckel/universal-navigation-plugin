package de.nielsjaeckel.confluence.plugins.universalnavigation.integration;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.util.validation.ValidationPattern;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.NavigationRenderer;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.ApplicationIntegrationService;
import de.nielsjaeckel.confluence.plugins.universalnavigation.service.RendererRegistry;
import org.dom4j.Element;

import static com.atlassian.plugin.util.validation.ValidationPattern.test;

public class NavigationRendererModuleDescriptor extends AbstractModuleDescriptor<NavigationRenderer> {

    private NavigationRenderer renderer;
    private RendererRegistry rendererRegistry;
    private ApplicationIntegrationService applicationIntegrationService;

    public NavigationRendererModuleDescriptor(ModuleFactory moduleFactory, RendererRegistry rendererRegistry, ApplicationIntegrationService applicationIntegrationService) {
        super(moduleFactory);
        this.rendererRegistry = rendererRegistry;
        this.applicationIntegrationService = applicationIntegrationService;
    }

    protected void provideValidationRules(final ValidationPattern pattern) {

        super.provideValidationRules(pattern);

        pattern.rule(test("@name").withError("The name is required"));
        pattern.rule(test("@class").withError("The class is required"));
    }

    @Override
    public void init(Plugin plugin, Element element) throws PluginParseException {

        super.init(plugin, element);

        // get attributes
        String name = element.attributeValue("name");
        String clazz = element.attributeValue("class");
        String example = element.elementText("example");

        // load class  with plugin's classloader
        try {
            ClassLoader pluginClassLoader = plugin.getClassLoader();
            Class rendererClass = pluginClassLoader.loadClass(clazz);

            renderer = (NavigationRenderer) rendererClass.newInstance();
            renderer.setId(name);
            renderer.setDescription(this.getDescription()); // the super call fills this property
            renderer.setExample(example);
            renderer.setProvidingPlugin(plugin);
            renderer.setApplicationIntegrationService(applicationIntegrationService);
        }
        catch (Exception e) {

            throw new PluginParseException(String.format("Could not load renderer '%s'.", name), e);
        }
    }

    @Override
    public NavigationRenderer getModule() {
        return renderer;
    }

    @Override
    public void enabled() {
        super.enabled();
        rendererRegistry.registerRenderer(renderer);
    }

    @Override
    public void disabled() {
        super.disabled();
        rendererRegistry.unregisterRenderer(renderer);
    }

    @Override
    public void destroy( Plugin plugin ) {
        super.destroy(plugin);
        rendererRegistry.unregisterRenderer(renderer);
    }
}
