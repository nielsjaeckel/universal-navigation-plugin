package de.nielsjaeckel.confluence.plugins.universalnavigation.integration;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.util.validation.ValidationPattern;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.ApplicationIntegrationService;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.ApplicationType;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.NodeParameter;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.NodeType;
import de.nielsjaeckel.confluence.plugins.universalnavigation.service.NodeTypeRegistry;
import org.apache.commons.lang.StringUtils;
import org.dom4j.Element;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static com.atlassian.plugin.util.validation.ValidationPattern.test;

/**
 * The type NodeTypeModuleDescriptor
 */
public class NodeTypeModuleDescriptor extends AbstractModuleDescriptor<NodeType> {

    private NodeType nodeType;
    private NodeTypeRegistry nodeTypeRegistry;
    private ApplicationIntegrationService applicationIntegrationService;


    public NodeTypeModuleDescriptor(ModuleFactory moduleFactory, NodeTypeRegistry nodeTypeRegistry, ApplicationIntegrationService applicationIntegrationService) {
        super(moduleFactory);
        this.nodeTypeRegistry = nodeTypeRegistry;
        this.applicationIntegrationService = applicationIntegrationService;
    }

    protected void provideValidationRules(final ValidationPattern pattern) {

        super.provideValidationRules(pattern);

        pattern.rule(test("@name").withError("The name is required"));
        pattern.rule(test("@class").withError("The class is required"));
        pattern.rule(test("@supportedApplications").withError("The list of supported applications (supportedApplications) is required"));
    }

    @Override
    public void init(Plugin plugin, Element element) throws PluginParseException {

        super.init(plugin, element);

        // get attributes
        String id = element.attributeValue("name");
        String clazz = element.attributeValue("class");
        String template = element.attributeValue("template");
        String example = element.elementText("example");
        String supportedApplicationsParam = element.attributeValue("supportedApplications");

        // load class and template with plugin's classloader
        try {

            // validate application for this nodetype
            List<ApplicationType> supportedApplications = parseSupportedApplications(supportedApplicationsParam);
            boolean isValidApplication = supportedApplications.contains(applicationIntegrationService.getApplicationType());

            if (isValidApplication) {

                ClassLoader pluginClassLoader = plugin.getClassLoader();
                Class nodeTypeClass = pluginClassLoader.loadClass(clazz);
                InputStream templateStream = null;
                if (StringUtils.isNotBlank(template)) {
                    templateStream = pluginClassLoader.getResourceAsStream(template);
                }

                // check if template exists
                if (templateStream == null) {
                    throw new PluginParseException(String.format("Template '%s' not found.", template));
                }

                // load template
                String renderTemplate = new java.util.Scanner(templateStream, "UTF-8").useDelimiter("\\A").next();
                templateStream.close();

                // load node parameters
                List<NodeParameter> nodeParameters = getNodeParameters(element);

                // build the final NodeType object
                nodeType = new NodeType(id, nodeTypeClass);
                nodeType.setDescription(this.getDescription()); // the super call fills this property
                nodeType.setExample(example);
                nodeType.setRenderTemplate(renderTemplate);
                nodeType.setProvidingPlugin(plugin);
                nodeType.setSupportedApplications(supportedApplications);
                nodeType.setNodeParameters(nodeParameters);
            }
            else {
                // not valid application: fail silently
                nodeType = null;
            }
        }
        catch (Exception e) {

            throw new PluginParseException(String.format("Could not parse node type '%s'.", id), e);
        }
    }

    private List<ApplicationType> parseSupportedApplications(String parameter) {

        List<ApplicationType> supportedApplications = new ArrayList<ApplicationType>();

        String[] parameterParts = parameter.split(",");
        for (String parameterPart : parameterParts) {

            parameterPart = parameterPart.toUpperCase().trim();

            try {
                supportedApplications.add(ApplicationType.valueOf(parameterPart));
            }
            catch (IllegalArgumentException e) {
                throw new IllegalArgumentException(String.format("The application type '%s' is unknown.", parameterPart), e);
            }
        }

        return supportedApplications;
    }

    private List<NodeParameter> getNodeParameters(Element element) {

        List<NodeParameter> nodeParameters = new ArrayList<NodeParameter>();

        for(Object paramObject : element.elements("param")) {

            Element paramElement = (Element) paramObject;
            String parameterName = paramElement.attributeValue("name").trim(); // the name attribute must always be given. NPE here is fine.
            String parameterDescription = paramElement.attributeValue("description");
            String parameterMandatory = paramElement.attributeValue("mandatory");

            // build parameter object
            NodeParameter param = new NodeParameter(parameterName);

            if (parameterDescription != null) {
                param.setDescription(parameterDescription.trim());
            }

            if (parameterMandatory != null) {
                param.setMandatory("true".equalsIgnoreCase(parameterMandatory.trim()));
            }

            // add to list
            nodeParameters.add(param);
        }

        return nodeParameters;
    }

    @Override
    public NodeType getModule() {
        return nodeType;
    }

    @Override
    public String getName() {

        String name = super.getName();

        if (nodeType == null) {
            name += " (Not available for this application type)";
        }

        return name;
    }

    @Override
    public void enabled() {
        super.enabled();

        if (nodeType != null) {
            nodeTypeRegistry.registerNodeType(nodeType);
        }
    }

    @Override
    public void disabled() {
        super.disabled();

        if (nodeType != null) {
            nodeTypeRegistry.unregisterNodeType(nodeType);
        }
    }

    @Override
    public void destroy( Plugin plugin ) {
        super.destroy(plugin);

        if (nodeType != null) {
            nodeTypeRegistry.unregisterNodeType(nodeType);
        }
    }
}
