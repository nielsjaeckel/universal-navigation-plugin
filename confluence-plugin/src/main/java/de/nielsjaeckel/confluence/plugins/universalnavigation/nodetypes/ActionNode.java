package de.nielsjaeckel.confluence.plugins.universalnavigation.nodetypes;

import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.user.User;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.ApplicationIntegrationService;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.BaseNode;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.ConfluenceNodeContext;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.NodeContext;

import javax.servlet.http.HttpServletRequest;

/**
 * The type ActionNode
 */
public class ActionNode extends BaseNode {

    private String actionUri;
    private String title;
    private transient boolean active = false;

    public String getActionUri() {
        return actionUri;
    }

    public void setActionUri(String actionUri) {
        this.actionUri = actionUri;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public void init(NodeContext nodeContext, ApplicationIntegrationService applicationIntegrationService) throws Exception {

        active = ((ConfluenceNodeContext) nodeContext).currentActionEqualsTargetAction(actionUri);
    }

    @Override
    public boolean isActive() {
        return active;
    }
}
