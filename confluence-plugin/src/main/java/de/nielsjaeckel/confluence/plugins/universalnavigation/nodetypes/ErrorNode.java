package de.nielsjaeckel.confluence.plugins.universalnavigation.nodetypes;

import de.nielsjaeckel.confluence.plugins.universalnavigation.api.*;

/**
 * The type ErrorNode for internal use only
 */
public class ErrorNode extends BaseNode {

    private static NodeType nodeType = new NodeType("internalErrorNode", ErrorNode.class);

    static {

        nodeType.setRenderTemplate("<li class=\"node-${node.nodeType.id}\">\n" +
                "<span class=\"node-content\"><span>ERROR: ${node.title}</span></span>\n" +
                "${renderer.renderChildNodes($node)}\n" +
                "</li>");
    }

    private String title;
    private Throwable throwable;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Throwable getThrowable() {
        return throwable;
    }

    public void setThrowable(Throwable throwable) {
        this.throwable = throwable;
    }

    public void setNodeType(NodeType nodeType) {
        throw new UnsupportedOperationException();
    }

    public NodeType getNodeType() {
        return nodeType;
    }

    @Override
    public void validate() throws NodeValidationException {
        validateNotEmpty("title", title);
    }

    @Override
    public void init(NodeContext nodeContext, ApplicationIntegrationService applicationIntegrationService) throws Exception {
        // no initialization to do
    }

    @Override
    public boolean isActive() {
        // this node can never be active
        return false;
    }
}
