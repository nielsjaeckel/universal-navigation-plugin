package de.nielsjaeckel.confluence.plugins.universalnavigation.nodetypes;

import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.user.User;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.ApplicationIntegrationService;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.BaseNode;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.ConfluenceNodeContext;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.NodeContext;

/**
 * The type ExternalLinkNode
 */
public class ExternalLinkNode extends BaseNode {

    private String url;
    private String title;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public void init(NodeContext nodeContext, ApplicationIntegrationService applicationIntegrationService) throws Exception {

        // nothing to do here
    }

    @Override
    public boolean isActive() {
        return false;
    }
}
