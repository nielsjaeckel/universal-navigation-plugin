package de.nielsjaeckel.confluence.plugins.universalnavigation.nodetypes;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.Namespace;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.spring.container.ContainerManager;
import com.atlassian.user.User;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.ApplicationIntegrationService;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.BaseNode;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.ConfluenceNodeContext;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.NodeContext;

import javax.naming.OperationNotSupportedException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The type SpaceNode
 */
public class FavoritePagesListNode extends BaseNode {

    private String noFavoritesMessage = "Currently you do not have favorite pages";
    private int maxCount = 10;

    private transient List<AbstractPage> favorites = new ArrayList<AbstractPage>();
    private transient Map<AbstractPage, Boolean> pageActiveCache = new HashMap<AbstractPage, Boolean>();
    private transient boolean hasActiveFavoritePage = false;

    public String getNoFavoritesMessage() {
        return noFavoritesMessage;
    }

    public void setNoFavoritesMessage(String noFavoritesMessage) {
        this.noFavoritesMessage = noFavoritesMessage;
    }

    public int getMaxCount() {
        return maxCount;
    }

    public void setMaxCount(int maxCount) {
        this.maxCount = maxCount;
    }

    public List<AbstractPage> getFavorites() {
        return favorites;
    }

    @Override
    public void init(NodeContext nodeContext, ApplicationIntegrationService applicationIntegrationService) throws Exception {

        favorites = new ArrayList<AbstractPage>();
        User user = (User) nodeContext.getCurrentUser();

        // anonymous users cannot have favorites
        if (user == null) {
            return;
        }

        Label favoriteLabel = new Label(LabelManager.FAVOURITE_LABEL, Namespace.PERSONAL,
                user.getName());
        Label favouriteLabel = new Label(LabelManager.FAVOURITE_LABEL_YANKEE, Namespace.PERSONAL,
                user.getName());

        // get all space favorites
        LabelManager labelManager = (LabelManager) ContainerManager.getComponent("labelManager");
        List<ContentEntityObject> favoriteContent = (List<ContentEntityObject>) labelManager.getContentForLabel(favoriteLabel, maxCount);
        List<ContentEntityObject> favouriteContent = (List<ContentEntityObject>) labelManager.getContentForLabel(favouriteLabel, maxCount);

        // get one consolidated favorites list
        favoriteContent.addAll(favouriteContent);

        for (ContentEntityObject ceo : favoriteContent) {

            if (ceo instanceof AbstractPage) {

                AbstractPage ap_ceo = (AbstractPage) ceo;
                favorites.add(ap_ceo);

                // determine whether this node is active
                boolean active = ap_ceo.equals(((ConfluenceNodeContext) nodeContext).getCurrentPage());
                pageActiveCache.put(ap_ceo, active);

                // set to true if there is at least one active page
                if (active) {
                    hasActiveFavoritePage = true;
                }
            }
        }
    }

    public boolean isActive(AbstractPage page) {

        if (pageActiveCache.containsKey(page)) {
            return pageActiveCache.get(page);
        }

        return false;
    }

    @Override
    public boolean hasActiveDescendant() {
        return hasActiveFavoritePage;
    }

    @Override
    public boolean isActive() {
        return false;
    }

    @Override
    public void setHtmlId(String htmlId) {
        throw new RuntimeException(new OperationNotSupportedException("Defining a html ID for favorite pages list node is not possible."));
    }
}
