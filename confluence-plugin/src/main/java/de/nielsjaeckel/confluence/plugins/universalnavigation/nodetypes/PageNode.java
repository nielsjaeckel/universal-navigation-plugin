package de.nielsjaeckel.confluence.plugins.universalnavigation.nodetypes;

import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.user.User;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.*;

/**
 * The type SpaceNode
 */
public class PageNode extends BaseNode implements PermissibleNode {

    private String spaceKey;
    private String pageTitle;
    private String notPermittedMessage = "Not Permitted";
    private transient Page page;
    private transient boolean permitted = false;
    private transient boolean active = false;


    public String getSpaceKey() {
        return spaceKey;
    }

    public void setSpaceKey(String spaceKey) {
        this.spaceKey = spaceKey;
    }

    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    @Override
    public void init(NodeContext nodeContext, ApplicationIntegrationService applicationIntegrationService) throws Exception {

        // get space manager (throws ClassNotFoundException)
        PageManager pageManager = (PageManager) applicationIntegrationService.getComponent("com.atlassian.confluence.pages.PageManager");
        PermissionManager permissionManager = (PermissionManager) applicationIntegrationService.getComponent("com.atlassian.confluence.security.PermissionManager");

        // load page
        page = pageManager.getPage(spaceKey, pageTitle);

        // check page
        if (page == null) {
            throw new IllegalArgumentException(String.format("Could not find page with title '%s' in space '%s'.", pageTitle, spaceKey));
        }

        // check permissions
        permitted = permissionManager.hasPermission((User) nodeContext.getCurrentUser(), Permission.VIEW, page);

        // determine whether this node is active
        active = page.equals(((ConfluenceNodeContext) nodeContext).getCurrentPage());
    }

    public Page getPage() {
        return page;
    }

    public boolean isPermitted() {
        return permitted;
    }

    @Override
    public boolean hasPermittedDescendants() {
        return PermissibleNode.Helper.hasPermittedDescendants(this);
    }

    @Override
    public boolean isActive() {
        return active;
    }

    public String getNotPermittedMessage() {
        return notPermittedMessage;
    }

    public void setNotPermittedMessage(String notPermittedMessage) {
        this.notPermittedMessage = notPermittedMessage;
    }
}
