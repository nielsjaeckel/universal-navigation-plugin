package de.nielsjaeckel.confluence.plugins.universalnavigation.nodetypes;

import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.user.User;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.*;

/**
 * The type SpaceNode
 */
public class SpaceNode extends BaseNode implements PermissibleNode {

    private String spaceKey;
    private String notPermittedMessage = "Not Permitted";
    private transient Space space;
    private transient boolean permitted = false;
    private transient boolean active = false;


    public String getSpaceKey() {
        return spaceKey;
    }

    public void setSpaceKey(String spaceKey) {
        this.spaceKey = spaceKey;
    }

    @Override
    public void init(NodeContext nodeContext, ApplicationIntegrationService applicationIntegrationService) throws Exception {

        // get space manager (throws ClassNotFoundException)
        SpaceManager spaceManager = (SpaceManager) applicationIntegrationService.getComponent("com.atlassian.confluence.spaces.SpaceManager");
        PermissionManager permissionManager = (PermissionManager) applicationIntegrationService.getComponent("com.atlassian.confluence.security.PermissionManager");

        // load space
        space = spaceManager.getSpace(spaceKey);

        // check space
        if (space == null) {
            throw new IllegalArgumentException(String.format("Could not find space for space key '%s'.", spaceKey));
        }

        // check permissions
        permitted = permissionManager.hasPermission((User) nodeContext.getCurrentUser(), Permission.VIEW, space);

        // determine whether this node is active
        active = space.equals(((ConfluenceNodeContext) nodeContext).getCurrentSpace());
    }

    public Space getSpace() {
        return space;
    }

    public boolean isPermitted() {
        return permitted;
    }

    @Override
    public boolean hasPermittedDescendants() {
        return PermissibleNode.Helper.hasPermittedDescendants(this);
    }

    @Override
    public boolean isActive() {
        return active;
    }

    public String getNotPermittedMessage() {
        return notPermittedMessage;
    }

    public void setNotPermittedMessage(String notPermittedMessage) {
        this.notPermittedMessage = notPermittedMessage;
    }
}
