package de.nielsjaeckel.confluence.plugins.universalnavigation.nodetypes;

import de.nielsjaeckel.confluence.plugins.universalnavigation.api.ApplicationIntegrationService;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.BaseNode;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.NodeContext;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.NodeValidationException;

/**
 * The type TextNode
 */
public class TextNode extends BaseNode {

    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public void init(NodeContext nodeContext, ApplicationIntegrationService applicationIntegrationService) throws Exception {
        // no initialization to do
    }

    @Override
    public boolean isActive() {
        // this node can never be active
        return false;
    }

}
