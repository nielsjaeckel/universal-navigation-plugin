package de.nielsjaeckel.confluence.plugins.universalnavigation.service;

import org.dom4j.DocumentException;

/**
 * The type NavigationException
 */
public class NavigationException extends Exception {

    public NavigationException(String s) {
        super(s);
    }

    public NavigationException(String s, Throwable t) {
        super(s, t);
    }
}
