package de.nielsjaeckel.confluence.plugins.universalnavigation.service;

import com.atlassian.util.profiling.UtilTimerStack;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.Navigation;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.NavigationNode;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.NodeContext;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.NodeVisibility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NavigationVisibilityHelper {

    private Map<NodeVisibility, Boolean> visibilityCache = new HashMap<NodeVisibility, Boolean>();

    public NavigationVisibilityHelper(NodeContext nodeContext) {

        // initialize all visibilities
        for(NodeVisibility visibility : NodeVisibility.values()) {

            visibilityCache.put(visibility, visibility.isVisible(nodeContext));
        }
    }

    private boolean isVisible(NavigationNode node) {
        return visibilityCache.get(node.getVisibility());
    }

    public void removeInvisibleNodes(Navigation navigation) {

        String timerName = "executing NavigationVisibilityHelper.removeInvisibleNodes()";
        UtilTimerStack.push(timerName);

        try {

            NavigationNode rootNode = navigation.getRootNode();

            // remove invisible children if root not itself is visible
            if (isVisible(rootNode)) {
                removeInvisibleChildNodes(rootNode);
            }
            else {

                // remove all child nodes if root node itself is not visible
                unlinkNodes(new ArrayList<NavigationNode>(rootNode.getChildNodes()));
            }
        }
        finally {
            UtilTimerStack.pop(timerName);
        }
    }

    private void removeInvisibleChildNodes(NavigationNode node) {

        List<NavigationNode> nodesToRemove = new ArrayList<NavigationNode>();

        for (NavigationNode childNode : node.getChildNodes()) {

            if (isVisible(childNode)) {

                removeInvisibleChildNodes(childNode);
            }
            else {

                // enqueue for later removal (cannot remove here, getting ConcurrentModificationExceptions)
                nodesToRemove.add(childNode);
            }
        }

        // now deleting nodes
        unlinkNodes(nodesToRemove);
    }


    private void unlinkNodes(List<NavigationNode> nodesToRemove) {

        for (NavigationNode childNode : nodesToRemove) {
            childNode.setParentNode(null);
        }
    }
}
