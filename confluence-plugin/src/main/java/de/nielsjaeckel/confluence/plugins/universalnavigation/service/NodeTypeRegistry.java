package de.nielsjaeckel.confluence.plugins.universalnavigation.service;


import com.atlassian.plugin.PluginAccessor;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.NavigationRenderer;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.NodeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class NodeTypeRegistry {

    private static final Logger LOG = LoggerFactory.getLogger(NodeTypeRegistry.class);

    private Map<String, NodeType> nodeTypes = new HashMap<String, NodeType>();
    private PluginAccessor pluginAccessor;


    public NodeTypeRegistry(PluginAccessor pluginAccessor) {
        this.pluginAccessor = pluginAccessor;
    }

    public void registerNodeType(NodeType nodeType) {

        LOG.info("registering node type: " + nodeType.getId());
        nodeTypes.put(nodeType.getId(), nodeType);
    }

    public void unregisterNodeType(NodeType nodeType) {

        LOG.info("unregistering node type: " + nodeType.getId());
        nodeTypes.remove(nodeType.getId());
    }

    public boolean hasNodeType(String id) {

        return nodeTypes.containsKey(id);
    }

    public NodeType getNodeType(String id) {

        return nodeTypes.get(id);
    }

    public Collection<NodeType> getRegisteredNodeTypes() {
        return nodeTypes.values();
    }
}