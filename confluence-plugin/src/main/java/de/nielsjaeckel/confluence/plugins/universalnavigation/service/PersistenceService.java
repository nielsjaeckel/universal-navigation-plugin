package de.nielsjaeckel.confluence.plugins.universalnavigation.service;

import com.atlassian.bandana.BandanaContext;
import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext;
import com.atlassian.sal.api.ApplicationProperties;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.ApplicationIntegrationService;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.Navigation;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.NodeContext;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public class PersistenceService {

    private static final Logger LOG = LoggerFactory.getLogger(PersistenceService.class);

    private static final String PERSISTENCE_DIRECTORY_NAME = "universal-navigation-plugin";
    private static final String MAIN_NAVIGATION_FILE_NAME = "main-navigation.xml";
    private static final String MAIN_NAVIGATION_FILE_NAME_TEMP = "main-navigation.new.xml";
    private static final String EXAMPLE_MAIN_NAVIGATION_FILE_NAME = "example-main-navigation.xml";

    private static final String MAIN_NAVIGATION_STYLE_BANDANA_KEY = "universal-navigation-plugin.main-navigation.style";
    private static final String MAIN_NAVIGATION_STYLE_DEFAULT = "topbar-basic";
    private static final BandanaContext BANDANA_CONTEXT = new ConfluenceBandanaContext();

    private ApplicationProperties applicationProperties;
    private File persistenceDirectory;
    private ApplicationIntegrationService applicationIntegrationService;
    private XmlNavigationService xmlNavigationService;
    private BandanaManager bandanaManager;


    public synchronized void storeMainNavigation(InputStream fis) throws PersistenceException {

        if (fis == null) {
            throw new IllegalArgumentException("uploadedFile cannot be null!");
        }

        // copy file to the file system
        File tempFile = new File(getPersistenceDirectory(), MAIN_NAVIGATION_FILE_NAME_TEMP);
        OutputStream fos = null;
        try {
            fos = new FileOutputStream(tempFile);
            IOUtils.copy(fis, fos);

        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(), e);
        }
        finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    LOG.error("Error while storing main navigation in file system", e);
                }
            }
        }


        // validate by reading navigation
        NodeContext nodeContext = applicationIntegrationService.getCurrentNodeContext();
        InputStream mainNavigationStream = null;
        try {
            mainNavigationStream = new FileInputStream(tempFile);
            Navigation navigation = xmlNavigationService.parseXmlNavigation(mainNavigationStream, nodeContext, XmlNavigationService.ParseMode.STRICT);

        } catch (FileNotFoundException e) {
            throw new PersistenceException(e.getMessage(), e);
        } catch (NavigationException e) {
            throw new PersistenceException(e.getMessage(), e);
        } finally {
            if (mainNavigationStream != null) {
                try {
                    mainNavigationStream.close();
                } catch (IOException e) {
                    LOG.error("Error closing temp main navigation file.", e);
                }
            }
        }

        // no failure until here, so move temp file to final destination
        File finalFile = new File(getPersistenceDirectory(), MAIN_NAVIGATION_FILE_NAME);

        if (finalFile.exists()) {
            finalFile.delete();
        }

        if (!tempFile.renameTo(finalFile)) {
            throw new PersistenceException("Could not rename main navigation file.");
        }
    }



    public synchronized void deleteMainNavigation() {

        File mainNavigation = new File(getPersistenceDirectory(), MAIN_NAVIGATION_FILE_NAME);
        mainNavigation.delete();
    }



    public synchronized InputStream readMainNavigation() throws PersistenceException {

        File mainNavigation = new File(getPersistenceDirectory(), MAIN_NAVIGATION_FILE_NAME);

        if (! mainNavigation.exists()) {
            return null;
        }

        try {
            return new FileInputStream(mainNavigation);
        } catch (FileNotFoundException e) {
            throw new PersistenceException(e);
        }
    }


    public File getPersistenceDirectory() {

        if (persistenceDirectory == null) {

            File tempPersistenceDir = new File(applicationProperties.getHomeDirectory(), PERSISTENCE_DIRECTORY_NAME);
            if (!tempPersistenceDir.exists()) {
                initializePersistenceDirectory(tempPersistenceDir);
            }

            persistenceDirectory = tempPersistenceDir;
        }

        return persistenceDirectory;
    }

    private void initializePersistenceDirectory(File tempPersistenceDir) {

        // create directory
        if (!tempPersistenceDir.mkdirs()) {
            throw new PersistenceException("Could not create persistence directory: " + tempPersistenceDir.getPath());
        }

        // create example navigation
        ClassLoader classLoader = this.getClass().getClassLoader();
        InputStream exampleNavigation = classLoader.getResourceAsStream(EXAMPLE_MAIN_NAVIGATION_FILE_NAME);
        storeMainNavigation(exampleNavigation);
    }

    public String getMainNavigationStyle() {
        String mainNavigationStyle = (String) bandanaManager.getValue(BANDANA_CONTEXT, MAIN_NAVIGATION_STYLE_BANDANA_KEY);

        // if there is no setting use the default style
        if (mainNavigationStyle == null) {
            mainNavigationStyle = MAIN_NAVIGATION_STYLE_DEFAULT;
        }

        return mainNavigationStyle;
    }

    public void setMainNavigationStyle(String newStyle) {
        bandanaManager.setValue(BANDANA_CONTEXT, MAIN_NAVIGATION_STYLE_BANDANA_KEY, newStyle);
    }


    public void setApplicationProperties(ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
    }

    public void setApplicationIntegrationService(ApplicationIntegrationService applicationIntegrationService) {
        this.applicationIntegrationService = applicationIntegrationService;
    }

    public void setXmlNavigationService(XmlNavigationService xmlNavigationService) {
        this.xmlNavigationService = xmlNavigationService;
    }

    public void setBandanaManager(BandanaManager bandanaManager) {
        this.bandanaManager = bandanaManager;
    }

    public static class PersistenceException extends RuntimeException {

        public PersistenceException(String s) {
            super(s);
        }

        public PersistenceException(Throwable t) {
            super(t);
        }

        public PersistenceException(String s, Throwable t) {
            super(s, t);
        }
    }
}
