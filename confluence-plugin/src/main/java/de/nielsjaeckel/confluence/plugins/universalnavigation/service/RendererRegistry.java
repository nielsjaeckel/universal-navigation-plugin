package de.nielsjaeckel.confluence.plugins.universalnavigation.service;

import de.nielsjaeckel.confluence.plugins.universalnavigation.api.NavigationRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RendererRegistry {

    private static final Logger LOG = LoggerFactory.getLogger(RendererRegistry.class);

    private Map<String, NavigationRenderer> renderers = new HashMap<String, NavigationRenderer>();


    public void registerRenderer(NavigationRenderer renderer) {

        LOG.info("registering renderer: " + renderer.getId());
        renderers.put(renderer.getId(), renderer);
    }

    public void unregisterRenderer(NavigationRenderer renderer) {

        LOG.info("unregistering renderer: " + renderer.getId());
        renderers.remove(renderer.getId());
    }

    public boolean hasRenderer(String id) {

        return renderers.containsKey(id);
    }

    public NavigationRenderer getRenderer(String id) {

        return renderers.get(id);
    }

    public Collection<NavigationRenderer> getRegisteredRenderers() {
        return renderers.values();
    }
}