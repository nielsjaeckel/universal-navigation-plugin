package de.nielsjaeckel.confluence.plugins.universalnavigation.service;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.util.profiling.UtilTimerStack;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.*;
import de.nielsjaeckel.confluence.plugins.universalnavigation.nodetypes.ErrorNode;
import org.apache.commons.beanutils.BeanUtils;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.*;

/**
 * The type XmlNavigationService
 */
public class XmlNavigationService {

    public static final String XML_ROOT_NODE_NAME = "navigation";
    public static final String XML_ROOT_NODE_ATTR_ID = "id";
    public static final String XML_ROOT_NODE_ATTR_DESCRIPTION = "description";
    public static final String XML_ROOT_NODE_ATTR_REVISION = "revision";
    public static final String XML_ELEMENT_PARAMETER_VISIBILITY = "visibility";
    public static final String XML_ELEMENT_PARAMETER = "parameter";
    public static final String XML_ELEMENT_PARAMETER_ATTR_NAME = "name";

    private static final Logger LOG = LoggerFactory.getLogger(XmlNavigationService.class);

    private PluginAccessor pluginAccessor;
    private NodeTypeRegistry nodeTypeRegistry;
    private ApplicationIntegrationService applicationIntegrationService;


    /**
     * Strategies how to deal with unknown nodes or errors during load
     */
    public static enum ParseMode {

        /**
         * Strategy: throw Exceptions
         */
        STRICT,

        /**
         * Strategy: log errors and wrap errorneous nodes in special objects so that the navigation can be displayed
         */
        LENIENT;
    }


    /**
     * Parsed the XML document with STRICT parse mode.
     *
     * @param xmlDocument
     * @return
     * @throws NavigationException
     */
    public Navigation parseXmlNavigation(InputStream xmlDocument, NodeContext nodeContext) throws NavigationException {
        return parseXmlNavigation(xmlDocument, nodeContext, ParseMode.STRICT);
    }


    public Navigation parseXmlNavigation(InputStream xmlDocument, NodeContext nodeContext, ParseMode parseMode) throws NavigationException {

        String timerName = "executing XmlNavigationService.parseXmlNavigation()";
        UtilTimerStack.push(timerName);

        try {
            // load document
            Document domDocument = createDocument(xmlDocument);

            // load and check root DOM node
            Element rootDomElement = domDocument.getRootElement();
            if (!XML_ROOT_NODE_NAME.equals(rootDomElement.getName())) {
                throw new NavigationException(String.format("Could not parse XML document. Expected node '%s' as root node.", XML_ROOT_NODE_NAME));
            }

            Navigation navi = new Navigation();

            // set meta data
            navi.setId(rootDomElement.attributeValue(XML_ROOT_NODE_ATTR_ID));
            navi.setDescription(rootDomElement.attributeValue(XML_ROOT_NODE_ATTR_DESCRIPTION));

            try {
                navi.setRevision(Integer.parseInt(rootDomElement.attributeValue(XML_ROOT_NODE_ATTR_REVISION)));
            }
            catch (NumberFormatException e) {

                LOG.warn("No navigation revision given. Fallback to revision 1.");
                navi.setRevision(1);
            }

            // build root node
            NavigationNode rootNode = new Navigation.RootNode();
            navi.setRootNode(rootNode);

            // load navigation recursively
            loadNavigation(rootDomElement, rootNode, parseMode, nodeContext);

            // set root node visibility
            Attribute attrVisibility = rootDomElement.attribute(XML_ELEMENT_PARAMETER_VISIBILITY);
            if (attrVisibility != null) {
                NodeVisibility visibility = NodeVisibility.valueOf(attrVisibility.getValue());
                rootNode.setVisibility(visibility);
            }

            return navi;
        }
        finally {
            UtilTimerStack.pop(timerName);
        }
    }

    private Document createDocument(final InputStream source) throws NavigationException {
        final SAXReader reader = new SAXReader();
        reader.setMergeAdjacentText(true);
        try {
            return reader.read(source);
        } catch (final DocumentException e) {
            throw new NavigationException("Cannot parse XML navigation.", e);
        }
    }

    private void loadNavigation(Element currentDomParent, NavigationNode targetParentNode, ParseMode parseMode, NodeContext nodeContext) throws NavigationException {

        for (final Iterator i = currentDomParent.elementIterator(); i.hasNext();) {

            final Element element = (Element) i.next();
            String elementName = element.getName();

            // parameters for parent node
            if (XML_ELEMENT_PARAMETER.equals(elementName)) {

                // set parameter at parent node
                setNodeParameter(targetParentNode, element.attributeValue(XML_ELEMENT_PARAMETER_ATTR_NAME), element.getTextTrim());

                // continue with next element
                continue;
            }

            // map element name to node type
            NavigationNode newNode = null;
            if (! nodeTypeRegistry.hasNodeType(elementName)) {

                newNode = handleRenderError(String.format("Could not handle unknown node type '%s'.", elementName), parseMode, null);
            }

            if (newNode == null) {
                NodeType nodeType = nodeTypeRegistry.getNodeType(elementName);

                // create new object
                try {
                    newNode = (NavigationNode) nodeType.getNodeTypeClass().newInstance();
                    newNode.setNodeType(nodeType);
                } catch (Exception e) {
                    newNode = handleRenderError(String.format("Error while instantiating node of type '%s': %s", elementName, e.getMessage()), parseMode, e);
                }

                // set parameters (= all attributes)
                for (final Iterator j = element.attributeIterator(); j.hasNext();) {
                    final Attribute attribute = (Attribute) j.next();
                    setNodeParameter(newNode, attribute.getName(), attribute.getValue());
                }

                // validate the node
                try {
                    newNode.validate();
                } catch (NodeValidationException e) {
                    newNode = handleRenderError(String.format("Invalid node of type '%s': %s", elementName, e.getMessage()), parseMode, e);
                }

                // initialize the node
                try {
                    newNode.init(nodeContext, applicationIntegrationService);
                } catch (Exception e) {
                    newNode = handleRenderError(String.format("Could not initialize node of type '%s': %s", elementName, e.getMessage()), parseMode, e);
                }
            }

            // add to parent
            targetParentNode.addChildNode(newNode);

            // recurse depth-first
            loadNavigation(element, newNode, parseMode, nodeContext);
        }
    }

    private void setNodeParameter(NavigationNode node, String parameter, String value) throws NavigationException {
        try {

            // special handling for the visibility parameter due to missing support for enums (BEANUTILS-346)
            if (XML_ELEMENT_PARAMETER_VISIBILITY.equals(parameter)) {

                NodeVisibility visibility = NodeVisibility.valueOf(value);
                BeanUtils.setProperty(node, "visibility", visibility);
            }
            else {

                BeanUtils.setProperty(node, parameter, value);
            }

        } catch (Exception e) {
            throw new NavigationException(String.format("Could not set property '%s' on node of type '%s' to value '%s'.", parameter, node.getClass().getName(), value), e);
        }
    }

    private NavigationNode handleRenderError(String message, ParseMode parseMode, Throwable t) throws NavigationException {

        if (parseMode.equals(ParseMode.STRICT)) {

            throw new NavigationException(message);
        }

        // else: lenient
        ErrorNode node = new ErrorNode();
        node.setTitle(message);
        node.setThrowable(t);

        LOG.warn("Error while loading navigation: " + message, t);

        return node;
    }

    public void setPluginAccessor(PluginAccessor pluginAccessor) {
        this.pluginAccessor = pluginAccessor;
    }

    public void setNodeTypeRegistry(NodeTypeRegistry nodeTypeRegistry) {
        this.nodeTypeRegistry = nodeTypeRegistry;
    }

    public void setApplicationIntegrationService(ApplicationIntegrationService applicationIntegrationService) {
        this.applicationIntegrationService = applicationIntegrationService;
    }
}
