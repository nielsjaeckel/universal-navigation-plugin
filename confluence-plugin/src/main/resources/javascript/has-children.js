AJS.toInit(function(){
    jQuery("div.universal-navigation ul li").each(function(){
        var li = jQuery(this);
        if (jQuery("ul li", li).length > 0) {
            li.addClass("has-children");
        }
    });
});
