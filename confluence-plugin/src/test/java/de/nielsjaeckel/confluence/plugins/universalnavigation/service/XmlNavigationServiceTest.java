package de.nielsjaeckel.confluence.plugins.universalnavigation.service;

import de.nielsjaeckel.confluence.plugins.universalnavigation.api.ApplicationIntegrationService;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.Navigation;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.NodeType;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.NavigationNode;
import de.nielsjaeckel.confluence.plugins.universalnavigation.nodetypes.TextNode;
import de.nielsjaeckel.confluence.plugins.universalnavigation.renderer.InspectRenderer;
import junit.framework.TestCase;
import org.mockito.Mockito;

import java.io.InputStream;
import java.util.List;

/**
 * The type XmlNavigationService
 */
public class XmlNavigationServiceTest extends TestCase {

    private XmlNavigationService naviService;
    private NodeTypeRegistry nodeTypeRegistry;
    private ApplicationIntegrationService applicationIntegrationService;

    public void setUp() {

        // build registries
        nodeTypeRegistry = new NodeTypeRegistry(null);
        applicationIntegrationService = Mockito.mock(ApplicationIntegrationService.class);

        // build navigation service
        naviService = new XmlNavigationService();
        naviService.setNodeTypeRegistry(nodeTypeRegistry);
        naviService.setApplicationIntegrationService(applicationIntegrationService);

        // register node types
        nodeTypeRegistry.registerNodeType(new NodeType("text", TextNode.class));
    }

    public void testXmlParser() throws NavigationException {

        InputStream xmlDocument = this.getClass().getClassLoader().getResourceAsStream("testnavigation1.xml");
        Navigation parsedNavigation = naviService.parseXmlNavigation(xmlDocument, null);

        // assert metadata
        assertEquals("test-navigation-1", parsedNavigation.getId());
        assertEquals(42, parsedNavigation.getRevision());
        assertEquals("This is the first test navigation", parsedNavigation.getDescription());

        // assert tree structure
        NavigationNode rootNode = parsedNavigation.getRootNode();
        List<NavigationNode> childNodes = rootNode.getChildNodes();

        assertEquals(3, childNodes.size());
        assertEquals("Test 1", ((TextNode) childNodes.get(0)).getTitle());
        assertEquals(3, childNodes.get(1).getChildNodes().size());
        assertEquals("Test 2.2", ((TextNode) childNodes.get(1).getChildNodes().get(1)).getTitle());

        // assert types
        assertEquals(childNodes.get(0).getClass().getSimpleName(), TextNode.class.getSimpleName());

        // output node structure
        InspectRenderer dr = new InspectRenderer();
        System.out.println(dr.renderNavigation(parsedNavigation, null, null));
    }
}
