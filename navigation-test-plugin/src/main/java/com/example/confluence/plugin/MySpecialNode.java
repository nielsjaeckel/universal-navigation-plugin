package com.example.confluence.plugin;

import de.nielsjaeckel.confluence.plugins.universalnavigation.api.ApplicationIntegrationService;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.BaseNode;
import de.nielsjaeckel.confluence.plugins.universalnavigation.api.NodeContext;

public class MySpecialNode extends BaseNode {

    private int attribute;

    public int getAttribute() {
        return attribute;
    }

    public void setAttribute(int attribute) {
        this.attribute = attribute;
    }

    @Override
    public void init(NodeContext nodeContext, ApplicationIntegrationService applicationIntegrationService) throws Exception {
        // no initialization to do
    }

    @Override
    public boolean isActive() {
        return false;
    }
}
