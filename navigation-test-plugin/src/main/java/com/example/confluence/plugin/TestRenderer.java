package com.example.confluence.plugin;

import de.nielsjaeckel.confluence.plugins.universalnavigation.api.*;

public class TestRenderer extends AbstractRenderer {

    @Override
    public CharSequence renderNavigation(Navigation navigation, NodeContext nodeContext, String style) {
        return String.format("This is the TestRenderer's output for the Navigation id:'%s' description:'%s'.", navigation.getId(), navigation.getDescription());
    }

    public CharSequence renderChildNodes(NavigationNode node) {
        return "this test renderer does not render child nodes.";
    }

    @Override
    public void setApplicationIntegrationService(ApplicationIntegrationService applicationIntegrationService) {
        // nothing
    }
}
