package de.nielsjaeckel.confluence.plugins.universalnavigation.api;

import com.atlassian.plugin.Plugin;

public abstract class AbstractRenderer implements NavigationRenderer {

    private String id;
    private String description;
    private Plugin providingPlugin;
    private String example;

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public Plugin getProvidingPlugin() {
        return providingPlugin;
    }

    @Override
    public void setProvidingPlugin(Plugin providingPlugin) {
        this.providingPlugin = providingPlugin;
    }

    @Override
    public String getExample() {
        return example;
    }

    @Override
    public void setExample(String exampleHtml) {
        this.example = exampleHtml;
    }
}
