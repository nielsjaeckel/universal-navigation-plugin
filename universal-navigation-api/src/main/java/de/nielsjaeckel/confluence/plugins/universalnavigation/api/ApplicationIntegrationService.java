package de.nielsjaeckel.confluence.plugins.universalnavigation.api;

import org.apache.velocity.VelocityContext;


/**
 * This interface provides application-dependent functionality
 */
public interface ApplicationIntegrationService {

    public VelocityContext getDefaultVelocityContext();

    public Object getComponent(String className) throws ClassNotFoundException;

    public ApplicationType getApplicationType();

    public NodeContext getCurrentNodeContext();
}
