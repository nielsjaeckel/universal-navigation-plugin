package de.nielsjaeckel.confluence.plugins.universalnavigation.api;

public enum ApplicationType {
    CONFLUENCE, JIRA;
}
