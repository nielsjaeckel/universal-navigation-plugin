package de.nielsjaeckel.confluence.plugins.universalnavigation.api;

import org.apache.commons.beanutils.BeanUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * The type BaseNode
 */
public abstract class BaseNode implements NavigationNode {

    private String cssClass;
    private String htmlId;

    private transient NavigationNode parentNode;
    private transient List<NavigationNode> childNodes = new ArrayList<NavigationNode>();
    private NodeType nodeType;
    private NodeVisibility visibility = NodeVisibility.always;

    @Override
    public NavigationNode getParentNode() {
        return parentNode;
    }

    @Override
    public void setParentNode(NavigationNode newParentNode) {

        // remove from parent's child list if necesary
        if (newParentNode == null) {
            if (parentNode.getChildNodes().contains(this)) {
                parentNode.removeChildNode(this);
            }
        }

        parentNode = newParentNode;
    }

    @Override
    public List<NavigationNode> getChildNodes() {
        return childNodes;
    }

    @Override
    public void addChildNode(NavigationNode childNode) {
        childNode.setParentNode(this);
        childNodes.add(childNode);
    }

    @Override
    public void removeChildNode(NavigationNode childNode) {
        childNodes.remove(childNode);
        childNode.setParentNode(null);
    }

    @Override
    public void setNodeType(NodeType nodeType) {
        this.nodeType = nodeType;
    }

    @Override
    public NodeType getNodeType() {
        return nodeType;
    }

    @Override
    public NodeVisibility getVisibility() {
        return visibility;
    }

    public void setVisibility(NodeVisibility visibility) {
        this.visibility = visibility;
    }

    public String getCssClass() {
        return cssClass;
    }

    public void setCssClass(String cssClass) {
        this.cssClass = cssClass;
    }

    public String getHtmlId() {
        return htmlId;
    }

    public void setHtmlId(String htmlId) {
        this.htmlId = htmlId;
    }

    @Override
    public void validate() throws NodeValidationException {

        // validate known mandatory parameters
        for (NodeParameter param : getNodeType().getNodeParameters()) {
            if (param.isMandatory()) {

                String parameterName = param.getName();
                String parameterValue = null;

                try {
                    parameterValue = BeanUtils.getProperty(this, parameterName);
                }
                catch (Exception e) {
                    throw new RuntimeException(String.format("Could not validate node of type '%s'.", getNodeType().getId()), e);
                }

                validateNotEmpty(parameterName, parameterValue);
            }
        }
    }

    public void validateNotEmpty(String parameterName, String parameterValue) throws NodeValidationException {

        if (parameterValue == null || "".equals(parameterValue)) {
            throw new NodeValidationException(String.format("The parameter '%s' cannot be empty.", parameterName));
        }
    }

    @Override
    public NavigationNode clone() {

        // base object clone
        NavigationNode clonedNode;
        try {
            Object cloned = super.clone();
            clonedNode = (NavigationNode) cloned;
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }

        // reset parent pointer
        clonedNode.setParentNode(null);

        // clone children
        for (NavigationNode childNode : this.getChildNodes()) {

            NavigationNode clonedChildNode = childNode.clone();
            clonedNode.addChildNode(clonedChildNode);
        }

        return clonedNode;
    }

    public boolean hasActiveDescendant() {

        for (NavigationNode childNode : getChildNodes()) {

            if (childNode.isActive() || childNode.hasActiveDescendant()) {

                return true;
            }
        }

        return false;
    }
}
