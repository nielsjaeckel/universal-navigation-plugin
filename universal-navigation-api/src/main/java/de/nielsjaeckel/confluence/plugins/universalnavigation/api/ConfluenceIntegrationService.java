package de.nielsjaeckel.confluence.plugins.universalnavigation.api;

import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.sal.api.component.ComponentLocator;
import org.apache.velocity.VelocityContext;

import java.io.File;
import java.util.Map;

public class ConfluenceIntegrationService implements ApplicationIntegrationService {

    @Override
    public ApplicationType getApplicationType() {
        return ApplicationType.CONFLUENCE;
    }

    @Override
    public VelocityContext getDefaultVelocityContext() {

        Map<String, Object> context = MacroUtils.defaultVelocityContext();
        context.put("applicationIntegrationService", this);

        return new VelocityContext(context);
    }

    @Override
    public Object getComponent(String className) throws ClassNotFoundException {
        Class componentClass = Class.forName(className);
        return ComponentLocator.getComponent(componentClass);
    }

    @Override
    public NodeContext getCurrentNodeContext() {
        return new ConfluenceNodeContext();
    }
}
