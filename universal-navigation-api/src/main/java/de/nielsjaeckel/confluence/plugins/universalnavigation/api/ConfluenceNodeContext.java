package de.nielsjaeckel.confluence.plugins.universalnavigation.api;

import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.spring.container.ContainerManager;
import com.atlassian.user.User;
import com.opensymphony.util.TextUtils;
import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.xwork.Action;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;

public class ConfluenceNodeContext implements NodeContext {

    private static final Logger LOG = LoggerFactory.getLogger(ConfluenceNodeContext.class);

    @Nullable
    private User currentUser;

    @Nullable
    private Action currentAction;

    @Nullable
    private HttpServletRequest currentRequest;

    private boolean isAdmin = false;
    private boolean isDashboard = false;
    private Space currentSpace;
    private Page currentPage;

    private PermissionManager permissionManager;
    private SpaceManager spaceManager;
    private PageManager pageManager;


    public ConfluenceNodeContext() {

        spaceManager = (SpaceManager) ContainerManager.getComponent("spaceManager");
        pageManager = (PageManager) ContainerManager.getComponent("pageManager");
        permissionManager = (PermissionManager) ContainerManager.getComponent("permissionManager");

        currentUser = AuthenticatedUserThreadLocal.getUser();

        try {
            currentAction = ServletActionContext.getContext().getActionInvocation().getAction();
        }
        catch (NullPointerException e) {
            // nothing, action not available in main.vmd context for example
        }

        currentRequest = ServletActionContext.getRequest();

        if (currentUser != null) {
            isAdmin = permissionManager.isConfluenceAdministrator(currentUser);
        }

        if (currentRequest != null) {

            // get current space
            String spaceKey = currentRequest.getParameter("spaceKey");
            if (spaceKey == null) {
                spaceKey = currentRequest.getParameter("key");
            }
            if (spaceKey != null) {
                currentSpace = spaceManager.getSpace(spaceKey);
            }

            // get current page
            String pageId = currentRequest.getParameter("pageId");
            if (TextUtils.stringSet(pageId)) {
                try {
                    currentPage = pageManager.getPage(Long.parseLong(pageId));
                }
                catch (NumberFormatException nfe) {
                    // nothing to do here, pageId could not be parsed, so currentPage stays null
                }
            }
            else {

                String pageTitle = currentRequest.getParameter("title");
                if (TextUtils.stringSet(spaceKey) && TextUtils.stringSet(pageTitle)) {
                    currentPage = pageManager.getPage(spaceKey, pageTitle);
                }
            }
        }
        else {
            LOG.info("There is no current request for context initialization.");
        }

        // check if dashboard
        isDashboard = currentActionEqualsTargetAction("/dashboard.action");
    }

    public boolean currentActionEqualsTargetAction(String targetAction) {

        // check if request is null
        if (currentRequest == null) {

            LOG.info("There is no current request for action checking.");
            return false;
        }

        String currentActionUri = currentRequest.getRequestURI();

        // remove possible anchor parts from this node's action uri
        String targetActionUri = currentRequest.getContextPath() + targetAction;
        if (targetActionUri.contains("#")) {
            targetActionUri = targetActionUri.substring(0, targetActionUri.lastIndexOf("#"));
        }

        // remove possible parameter parts from this node's action uri
        if (targetActionUri.contains("?")) {
            targetActionUri = targetActionUri.substring(0, targetActionUri.lastIndexOf("?"));
        }

        // determine whether this node is active
        return targetActionUri.equals(currentActionUri);
    }

    // use this method when rendering a navigation inside the main.vmd decorator, that doesn't have any other context
    public void provideContext(String spaceKey, String pageId) throws NumberFormatException {
        provideContext(spaceKey, Long.parseLong(pageId));
    }

    // use this method when rendering a navigation inside the main.vmd decorator, that doesn't have any other context
    public void provideContext(String spaceKey, long pageId) {
        currentSpace = spaceManager.getSpace(spaceKey);
        currentPage = pageManager.getPage(pageId);
    }

    @Override
    public Object getCurrentUser() {
        return currentUser;
    }

    @Override
    public boolean currentUserIsAdmin() {
        return isAdmin;
    }

    @Override
    @Nullable
    public Object getCurrentAction() {
        return currentAction;
    }

    @Override
    public boolean currentActionIsDashboard() {
        return isDashboard;
    }

    @Override
    public HttpServletRequest getCurrentRequest() {
        return currentRequest;
    }

    @Nullable
    public Space getCurrentSpace() {
        return currentSpace;
    }

    @Nullable
    public AbstractPage getCurrentPage() {
        return currentPage;
    }
}
