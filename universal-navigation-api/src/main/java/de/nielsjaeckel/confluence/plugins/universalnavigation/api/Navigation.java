package de.nielsjaeckel.confluence.plugins.universalnavigation.api;

/**
 * This class represents an entire navigation that is identified by an ID, that has a revision
 * and maybe a description and of course a root node of the actual navigation structure.
 * The type Navigation
 */
public class Navigation {

    private NavigationNode rootNode;
    private String id;
    private int revision;
    private String description;

    public NavigationNode getRootNode() {
        return rootNode;
    }

    public void setRootNode(NavigationNode rootNode) {
        this.rootNode = rootNode;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public static class RootNode extends BaseNode {

        @Override
        public void validate() throws NodeValidationException {
            // nothing to validate
        }

        @Override
        public void init(NodeContext nodeContext, ApplicationIntegrationService applicationIntegrationService) throws Exception {
            // nothing to initialize
        }

        @Override
        public boolean isActive() {
            // this node can never be active
            return false;
        }
    }
}
