package de.nielsjaeckel.confluence.plugins.universalnavigation.api;

import java.util.List;

/**
 * The type NavigationNode
 *
 * Node Lifecycle:
 *   0) new Instance is created
 *   1) all property setters are called
 *   2) node is being validated (validate Method)
 *   3) (optional) existing node gets cloned (clone Method)
 *   5) node is being initialized (init Method) <-- here the new context is given
 *   6) invisible nodes are removed (getVisibility() method)
 *
 * Method purposes:
 *    validate: validate this node's properties (e.g. coming from XML file)
 *    init: load additional objects based on the properties
 *    clone: copy all properties
 */
public interface NavigationNode {

    public NavigationNode getParentNode();

    public void setParentNode(NavigationNode parentNode);

    public List<NavigationNode> getChildNodes();

    public void addChildNode(NavigationNode childNode);

    public void removeChildNode(NavigationNode childNode);

    public void setNodeType(NodeType nodeType);

    public NodeType getNodeType();

    /**
     * Every node must be cloneable
     *
     * @return a completely independent clone (for caching and other users)
     */
    public NavigationNode clone();

    // cannot be used to validate on the node hierarchy as every node is validated and initialized separately. If
    // such a feature is needed some time, we need to introduce some kind of NavigationValidator or so.
    public void validate() throws NodeValidationException;

    public void init(NodeContext nodeContext, ApplicationIntegrationService applicationIntegrationService) throws Exception;

    public NodeVisibility getVisibility();

    public void setVisibility(NodeVisibility nodeVisibility);

    public boolean isActive();

    public boolean hasActiveDescendant();
}
