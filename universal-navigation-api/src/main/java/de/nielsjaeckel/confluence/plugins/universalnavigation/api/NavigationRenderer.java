package de.nielsjaeckel.confluence.plugins.universalnavigation.api;

import com.atlassian.plugin.Plugin;

public interface NavigationRenderer {

    public void setId(String id);

    public String getId();

    public void setDescription(String description);

    public String getDescription();

    public Plugin getProvidingPlugin();

    public String getExample();

    public void setExample(String example);

    public void setProvidingPlugin(Plugin providingPlugin);

    public CharSequence renderNavigation(Navigation navigation, NodeContext nodeContext, String style);

    public CharSequence renderChildNodes(NavigationNode node);

    public void setApplicationIntegrationService(ApplicationIntegrationService applicationIntegrationService);
}