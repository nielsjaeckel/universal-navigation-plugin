package de.nielsjaeckel.confluence.plugins.universalnavigation.api;

import javax.servlet.http.HttpServletRequest;

public interface NodeContext {

    // returns null when no user is logged in
    public Object getCurrentUser();

    public boolean currentUserIsAdmin();

    public Object getCurrentAction();

    public boolean currentActionIsDashboard();

    public HttpServletRequest getCurrentRequest();
}
