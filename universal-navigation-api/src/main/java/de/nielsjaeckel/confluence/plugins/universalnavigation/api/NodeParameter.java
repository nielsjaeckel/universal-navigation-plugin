package de.nielsjaeckel.confluence.plugins.universalnavigation.api;


public class NodeParameter {

    private String name;
    private String description = "";
    private boolean mandatory = false;


    public NodeParameter(String name) {
        setName(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isMandatory() {
        return mandatory;
    }

    public void setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
    }
}
