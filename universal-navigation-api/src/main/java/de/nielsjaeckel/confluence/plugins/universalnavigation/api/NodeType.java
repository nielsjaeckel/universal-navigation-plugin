package de.nielsjaeckel.confluence.plugins.universalnavigation.api;

import com.atlassian.plugin.Plugin;

import javax.annotation.Nullable;
import java.util.Collections;
import java.util.List;

/**
 * The type NodeType
 */
public class NodeType {

    private String id;
    private Class nodeTypeClass;

    // a velocity template to render the node
    private String renderTemplate;
    private List<ApplicationType> supportedApplications = Collections.emptyList();
    private List<NodeParameter> nodeParameters = Collections.emptyList();
    private Plugin providingPlugin;
    private String description;
    private String example;

    public NodeType(String id, Class nodeTypeClass) {
        this.id = id;
        this.nodeTypeClass = nodeTypeClass;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Class getNodeTypeClass() {
        return nodeTypeClass;
    }

    public void setNodeTypeClass(Class nodeClass) {
        this.nodeTypeClass = nodeClass;
    }

    @Nullable
    public String getRenderTemplate() {
        return renderTemplate;
    }

    public void setRenderTemplate(String renderTemplate) {
        this.renderTemplate = renderTemplate;
    }

    @Nullable
    public Plugin getProvidingPlugin() {
        return providingPlugin;
    }

    public void setProvidingPlugin(Plugin providingPlugin) {
        this.providingPlugin = providingPlugin;
    }

    @Nullable
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ApplicationType> getSupportedApplications() {
        return supportedApplications;
    }

    public void setSupportedApplications(List<ApplicationType> supportedApplications) {
        this.supportedApplications = supportedApplications;
    }

    public List<NodeParameter> getNodeParameters() {
        return this.nodeParameters;
    }

    public void setNodeParameters(List<NodeParameter> nodeParameters) {
        this.nodeParameters = nodeParameters;
    }

    @Nullable
    public String getExample() {
        return example;
    }

    public void setExample(String example) {
        this.example = example;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NodeType nodeType = (NodeType) o;

        if (id != null ? !id.equals(nodeType.id) : nodeType.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
