package de.nielsjaeckel.confluence.plugins.universalnavigation.api;

public class NodeValidationException extends Exception {

    public NodeValidationException(String message) {
        super(message);
    }

    @Override
    public synchronized Throwable fillInStackTrace() {

        // do not provide a stack trace
        return this;
    }
}
