package de.nielsjaeckel.confluence.plugins.universalnavigation.api;

public enum NodeVisibility {

    always {
        public boolean isVisible(NodeContext nodeContext) {
            return true;
        }
    },

    never {
        public boolean isVisible(NodeContext nodeContext) {
            return false;
        }
    },

    onlyAdmin {
        public boolean isVisible(NodeContext nodeContext) {
            return nodeContext.currentUserIsAdmin();
        }
    },

    onlyLoggedInUser {
        public boolean isVisible(NodeContext nodeContext) {
            return nodeContext.getCurrentUser() != null;
        }
    },

    onlyAnonymousUser {
        public boolean isVisible(NodeContext nodeContext) {
            return nodeContext.getCurrentUser() == null;
        }
    },

    onlyDashboard {
        public boolean isVisible(NodeContext nodeContext) {
            return nodeContext.currentActionIsDashboard();
        }
    },

    notOnDashboard {
        public boolean isVisible(NodeContext nodeContext) {
            return ! nodeContext.currentActionIsDashboard();
        }
    };


    public abstract boolean isVisible(NodeContext nodeContext);
}
