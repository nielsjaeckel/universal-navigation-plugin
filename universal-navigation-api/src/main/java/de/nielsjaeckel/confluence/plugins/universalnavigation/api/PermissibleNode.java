package de.nielsjaeckel.confluence.plugins.universalnavigation.api;

public interface PermissibleNode {

    public boolean isPermitted();

    public boolean hasPermittedDescendants();


    public static class Helper {

        public static boolean hasPermittedDescendants(NavigationNode node) {

            for (NavigationNode childNode : node.getChildNodes()) {

                if (childNode instanceof PermissibleNode) {

                    PermissibleNode permissibleChildNode = (PermissibleNode) childNode;
                    if (permissibleChildNode.isPermitted() || permissibleChildNode.hasPermittedDescendants()) {

                        // if we find any permitted node or child node the method returns true
                        return true;
                    }
                }
                else {

                    // if the node is not permissible, it is permitted by default
                    return true;
                }
            }

            // if all nodes were instanceof PermissibleNode and were not permitted, false is returned
            return false;
        }
    }
}
